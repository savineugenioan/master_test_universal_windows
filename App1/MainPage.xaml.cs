﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        ApplicationDataContainer setariLocale = ApplicationData.Current.LocalSettings;
        StorageFolder folderLocal = ApplicationData.Current.LocalFolder;
        StorageFile fisier;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Pagina2));
        }

        double CmToInch(double input)
        {
            return input / 2.54;
        }
        double CmToYard(double input)
        {
            return input / 91.44;
        }
        double CmToMile(double input)
        {
            return input / 160934.4;
        }
        double MToInch(double input)
        {
            return input / 0.0254;
        }
        double MToYard(double input)
        {
            return input * 1.0936;
        }
        double MToMile(double input)
        {
            return input / 1609.344;
        }
        double KmToInch(double input)
        {
            return input / 0.0000254;
        }
        double KmToYard(double input)
        {
            return input / 0.0009144;
        }
        double KmToMile(double input)
        {
            return input * 0.62137119;
        }
        private double MileToKm(double input)
        {
            return input * 1.609;
        }

        private double YardToKm(double input)
        {
            return input * 0.0009144;
        }

        private double InchToKm(double input)
        {
            return input *2.54/100000;
        }

        private double MileToM(double input)
        {
            return input * 1609;
        }

        private double YardToM(double input)
        {
            return input * 0.914;
        }

        private double InchToM(double input)
        {
            return input * 0.0254;
        }

        private double MileToCm(double input)
        {
            return input * 160934.4;
        }

        private double YardToCm(double input)
        {
            return input * 91.44;
        }

        private double InchToCm(double input)
        {
            return input * 2.54;
        }
        async void WriteToFile()
        {
            await FileIO.AppendTextAsync(fisier, Environment.NewLine+$"{TextBox1.Text} {cmbDin.SelectedItem.ToString()} {TextBox2.Text} {cmbDin1.SelectedItem.ToString()}");
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string from = cmbDin.SelectedItem.ToString();
            string into = cmbDin1.SelectedItem.ToString();
            if(TextBox1.Text.ToString().Trim() == "" || !double.TryParse(TextBox1.Text.ToString().Replace(',','a'), out double x))
            {
                MessageDialog dialog = new MessageDialog("Eroare text casuta 1", "Eroare");
                dialog.Commands.Add(new UICommand("Ok", null));
                dialog.DefaultCommandIndex = 0;
                dialog.CancelCommandIndex = 1;
                await dialog.ShowAsync();
                return;
            }
            switch (from)
            {
                case "CM" when into == "Inch": TextBox2.Text = Math.Round(CmToInch(Double.Parse(TextBox1.Text)), 2).ToString(); break;
                case "CM" when into == "Yard": TextBox2.Text = Math.Round(CmToYard(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "CM" when into == "Mile": TextBox2.Text = Math.Round(CmToMile(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "M" when into == "Inch": TextBox2.Text = Math.Round(MToInch(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "M" when into == "Yard": TextBox2.Text = Math.Round(MToYard(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "M" when into == "Mile": TextBox2.Text = Math.Round(MToMile(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "KM" when into == "Inch": TextBox2.Text = Math.Round(KmToInch(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "KM" when into == "Yard": TextBox2.Text = Math.Round(KmToYard(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "KM" when into == "Mile": TextBox2.Text = Math.Round(KmToMile(Double.Parse(TextBox1.Text)), 2).ToString();; break;

                case "Inch" when into == "CM": TextBox2.Text = Math.Round(InchToCm(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Yard" when into == "CM": TextBox2.Text = Math.Round(YardToCm(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Mile" when into == "CM": TextBox2.Text = Math.Round(MileToCm(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Inch" when into == "M": TextBox2.Text = Math.Round(InchToM(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Yard" when into == "M": TextBox2.Text = Math.Round(YardToM(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Mile" when into == "M": TextBox2.Text = Math.Round(MileToM(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Inch" when into == "KM": TextBox2.Text = Math.Round(InchToKm(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Yard" when into == "KM": TextBox2.Text = Math.Round(YardToKm(Double.Parse(TextBox1.Text)), 2).ToString();; break;
                case "Mile" when into == "KM": TextBox2.Text = Math.Round(MileToKm(Double.Parse(TextBox1.Text)), 2).ToString();; break;
            }
            WriteToFile();
        }

        private async void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            cmbDin.SelectedIndex = 0;
            cmbDin1.SelectedIndex = 0;
            cmbDin.SelectedItem = cmbDin.Items[0];
            cmbDin1.SelectedItem = cmbDin1.Items[0];
            fisier = await folderLocal.CreateFileAsync("dataFile.txt", CreationCollisionOption.OpenIfExists);
            if (setariLocale.Values.ContainsKey("Titlu_Main"))
            {
                string c = setariLocale.Values["Titlu_Main"].ToString();
                TextBoxTitlu.Text = c;
            }

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var list = cmbDin.Items.ToArray();
            var list1 = cmbDin1.Items.ToArray();

            cmbDin.Items.Clear();
            cmbDin1.Items.Clear();
            foreach (var elem in list1)
            {
                cmbDin.Items.Add(elem);
            }
            foreach (var elem in list)
            {
                cmbDin1.Items.Add(elem);
            }
            cmbDin.SelectedIndex = 0;
            cmbDin1.SelectedIndex = 0;
            cmbDin.SelectedItem = cmbDin.Items[0];
            cmbDin1.SelectedItem = cmbDin1.Items[0];
        }
    }
}
